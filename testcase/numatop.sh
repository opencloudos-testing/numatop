#!/bin/bash
###############################################################################
# @用例ID: 20231201-153742-788176072
# @用例名称: numatop
# @用例级别: 3
# @用例标签:
# @扩展属性:
# @用例类型: 功能测试
# @自动化: 1
# @超时时间: 0
# @用例描述: TODO: 简要描述用例测试的内容
###############################################################################
[ -z "$TST_TS_TOPDIR" ] && {
    TST_TS_TOPDIR="$(realpath "$(dirname "$0")/..")"
    export TST_TS_TOPDIR
}
source "${TST_TS_TOPDIR}/tst_common/lib/common.sh" || exit 1
###############################################################################

g_tmpdir="$(mktemp -d)"

tc_setup() {
    msg "this is tc_setup"

    # @预置条件:  检查numatop软件包是否存在
    assert_true yum install -y numatop
 
    return 0
}

do_test() {
    msg "this is do_test"

    # @测试步骤: 使用numatop执行帮助信息
    numatop -h
    # @预期结果: 成功status
    assert_true [ $? -eq 0 ]

    # @测试步骤: 测试numatop写入文件
    numatop -d  $g_tmpdir/test_out
    # @预期结果:
    assert_true [ -f  $g_tmpdir/test_out  ]

    # @测试步骤: 测试numatop按等级0输出
    numatop  -l 0 -f $g_tmpdir/test0
    # @预期结果:
    assert_true [ -f  $g_tmpdir/test0 ]

    # @测试步骤: 测试numatop按高精度采样输出
    numatop  -s high -f $g_tmpdir/test_high
    # @预期结果:
    assert_true [ -f  $g_tmpdir/test_high ]

    return 0
}

tc_teardown() {
    msg "this is tc_teardown"
    rm -rfv "$g_tmpdir" || return 1
    return 0
}

###############################################################################
tst_main "$@"
###############################################################################
