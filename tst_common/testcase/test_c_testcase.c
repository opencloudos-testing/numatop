/* ****************************************************************************
 * @用例ID: 20220418-230037-838974137
 * @用例名称: test_c_testcase
 * @用例级别: 3
 * @用例标签:
 * @扩展属性:
 * @用例类型: 功能测试
 * @自动化: 1
 * @超时时间: 0
 * @用例描述: TODO: 简要描述用例测试的内容
 * ***************************************************************************/

#include "main.h"

int tc_setup(int argc, char **argv) {
    msg("this is tc_setup");

    // @预置条件: TODO: 描述测试用例执行需要的预置条件
    // @预置条件: TODO: 可以使用skip_if_false等函数判断条件是否满足，不满足用例将不被执行
    skip_if_false(1 + 1 == 2); // TODO: 示例代码，用例中必须删除

    return 0;
}

int do_test(int argc, char **argv) {
    msg("this is do_test");

    // @测试步骤: TODO: 描述用例测试的操作步骤
    // @预期结果: TODO: 紧跟操作步骤描述该步骤操作后的预期结果，>>> 必须 <<<使用断言判断预期结果
    int a = 1 + 1;       // TODO: 示例代码，用例中必须删除
    assert_true(a == 2); // TODO: 示例代码，用例中必须删除

    // @测试步骤: step 2

    // @测试步骤: step 3
    // @预期结果: expect of step 3

    return 0;
}

int tc_teardown(int argc, char **argv) {
    msg("this is tc_teardown");
    return 0;
}
