#!/usr/bin/env python3
# coding: utf-8

import os.path
import sys

_suite_top_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
while _suite_top_dir != '/':
    if os.path.isfile(os.path.join(_suite_top_dir, 'lib', 'common.py')) or os.path.isfile(
            os.path.join(_suite_top_dir, 'tst_common', 'lib', 'common.py')):
        break
    _suite_top_dir = os.path.dirname(_suite_top_dir)
sys.path.append(_suite_top_dir)
from lib.common import TestCase


class PythonTestCase(TestCase):
    """
    @用例ID: 20220420-234008-893237761
    @用例名称: test_python_testcase
    @用例级别: 3
    @用例标签:
    @扩展属性:
    @用例类型: 功能测试
    @自动化: 1
    @超时时间: 0
    @用例描述: TODO: 简要描述用例测试的内容
    """

    def tc_setup(self, *args):
        self.msg("this is tc_setup")

        # @预置条件: TODO: 描述测试用例执行需要的预置条件
        # @预置条件: TODO: 可以使用skip_if_false等函数判断条件是否满足，不满足用例将不被执行
        self.skip_if_false(1 == 1)  # TODO: 示例代码，用例中必须删除

    def do_test(self, *args):
        self.msg("this is do_test")

        # @测试步骤: TODO: 描述用例测试的操作步骤
        # @预期结果: TODO: 紧跟操作步骤描述该步骤操作后的预期结果，>>> 必须 <<<使用断言判断预期结果
        a = 1 + 1  # TODO: 示例代码，用例中必须删除
        self.assert_true(a == 2)  # TODO: 示例代码，用例中必须删除

        # @测试步骤: step 2

        # @测试步骤: step 3
        # @预期结果: expect of step 3

    def tc_teardown(self, *args):
        self.msg("this is tc_teardown")


if __name__ == '__main__':
    PythonTestCase().tst_main(sys.argv)
