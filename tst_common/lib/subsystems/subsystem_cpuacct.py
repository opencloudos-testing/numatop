# -*- coding: utf-8 -*-

"""
@file    : subsystem_cpuacct.py
@time    : 2022-11-04 11:07:19
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from ._subsystem import Subsystem


class SubsystemCpuAcct(Subsystem):

    """
    cpuacct.stat
    cpuacct.usage
    cpuacct.usage_percpu
    """

    NAME = 'cpuacct'
    STATS = {
        'usage': int,
        'stat': dict,
        'usage_all': dict,
        'usage_sys': int,
        'usage_user': int,
        'usage_percpu': dict,
        'usage_percpu_sys': dict,
        'usage_percpu_user': dict}
