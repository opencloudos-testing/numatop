# -*- coding: utf-8 -*-

"""
@file    : subsystem_cpu.py
@time    : 2022-10-26 21:12:30
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import fileops
from ._subsystem import Subsystem


class SubsystemCpu(Subsystem):

    """
    Cpu CGroup subsystem. Provides access to

    cpu.cfs_period_us
    cpu.cfs_quota_us
    cpu.rt_period_us
    cpu.rt_runtime_us
    cpu.shares
    cpu.stat
    """

    NAME = 'cpu'
    _path_rt_period = '/proc/sys/kernel/sched_rt_period_us'
    _path_rt_runtime = '/proc/sys/kernel/sched_rt_runtime_us'
    CONFIGS = {
        'shares':        1024,
        # Are the default values correct?
        'rt_period_us':  int(fileops.read(_path_rt_period)),
        'rt_runtime_us': int(fileops.read(_path_rt_runtime)),
        'cfs_period_us': 100000,
        'cfs_quota_us': -1,
    }

    def parse(self):
        pass
