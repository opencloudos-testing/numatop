# -*- coding: utf-8 -*-

"""
@file    : subsystem_memory.py
@time    : 2022-11-04 11:07:39
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from ._subsystem import Subsystem


class SubsystemMemory(Subsystem):

    """
    Memory CGroup subsystem. Provides access to

    memory.failcnt
    memory.force_empty
    memory.limit_in_bytes
    memory.max_usage_in_bytes
    memory.memsw.failcnt
    memory.memsw.limit_in_bytes
    memory.memsw.max_usage_in_bytes
    memory.memsw.usage_in_bytes
    memory.move_charge_at_immigrate
    memory.numa_stat
    memory.oom_control
    memory.pressure_level
    memory.soft_limit_in_bytes
    memory.stat
    memory.swappiness
    memory.usage_in_bytes
    memory.use_hierarchy
    """

    NAME = 'memory'
    STATS = {
        'failcnt': int,
        'usage_in_bytes': int,
        'max_usage_in_bytes': int,
        'memsw.failcnt': int,
        'memsw.max_usage_in_bytes': int,
        'memsw.usage_in_bytes': int,
        'stat': dict,
        'numa_stat': dict,
        'kmem.tcp.failcnt': int,
        'kmem.tcp.max_usage_in_bytes': int,
        'kmem.tcp.usage_in_bytes': int,
        'kmem.failcnt': int,
        'kmem.max_usage_in_bytes': int,
        'kmem.usage_in_bytes': int,
        'kmem.slabinfo': dict,
    }
    MAX_ULONGLONG = 2 ** 63 - 1
    CONFIGS = {
        'limit_in_bytes': MAX_ULONGLONG,
        'memsw.limit_in_bytes': MAX_ULONGLONG,
        'move_charge_at_immigrate': 0,
        'oom_control': {'oom_kill_disable': 0, 'under_oom': 0},
        'soft_limit_in_bytes': MAX_ULONGLONG,
        'swappiness': 60,
        'use_hierarchy': 0,
        'kmem.tcp.limit_in_bytes': MAX_ULONGLONG,
        'kmem.limit_in_bytes': MAX_ULONGLONG,
    }
    CONTROLS = {
        'force_empty': None,
        'pressure_level': None,
    }
